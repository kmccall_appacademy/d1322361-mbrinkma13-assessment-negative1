# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.


def missing_numbers(nums)
  full_set = (nums.min..nums.max).to_a

  full_set - nums
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9

#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)

  base_10 = 0

  binary.to_s.reverse.chars.each_with_index do |ch, i|
    base_10 += (ch.to_i * 2 ** i)
  end

  base_10
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    result = Hash.new

    self.each do |k, v|
      if prc.call(k, v)
        result[k] = v
      end
    end

    result
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    result = Hash.new
    keys = (self.keys + hash.keys).uniq

    keys.each do |k|
      if (self[k] && hash[k]) && block_given?
        result[k] = yield(k, self[k], hash[k])
      elsif hash[k]
        result[k] = hash[k]
      else
        result[k] = self[k]
      end
    end

    result
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)

  first_two = [2, 1]

  if n >= 0 && n <= 1
    return first_two[n]
  elsif n >= 2
    while first_two.length < n + 1
      first_two << next_num_pos(first_two)
    end
  elsif n < 0
    while first_two.length < n.abs + 2
      first_two.unshift(next_num_neg(first_two))
    end
    return first_two[0]
  end

  first_two[n]
end


def next_num_pos(arr)
  arr[-2] + arr[-1]
end

def next_num_neg(arr)
  arr[1] - arr[0]
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  characters = string.chars
  i = string.length

  while i > 1
    palindromes = characters.each_cons(i).select { |p| p == p.reverse }
    if palindromes.empty? == false
      return palindromes.join.length
    end
    i -= 1
  end

  false
end
